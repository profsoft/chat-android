package ru.profsoft.localchat.data

import com.github.nkzawa.socketio.client.IO
import com.github.nkzawa.socketio.client.Socket
import ru.profsoft.localchat.data.model.User
import java.net.URISyntaxException

class SocketClient private constructor() {
    companion object {
        val instance : SocketClient by lazy { SocketClient() }
        var BASE_URL = "http://beta-chat.profsoft.online"
        const val SERVER_MESSAGE_KEY = "add mess"
        const val NEW_MESSAGE_KEY = "send mess"
        const val NEW_USERNAME_KEY = "new_username"
        const val USER_CONNECTED_KEY = "new_connected"
        const val USER_DISCONNECTED_KEY = "new_disconnected"
        const val UPDATE_GPS_KEY = "update_user_gps"
    }

    lateinit var socket: Socket
    var userId: String? = null
    var user: User? = null

    init {
        try {
            socket = IO.socket(BASE_URL)
        } catch (e: URISyntaxException) {
        }
    }
}