package ru.profsoft.localchat.data.model

import java.text.SimpleDateFormat
import java.util.*

abstract class AbstractChatMessage (
    val time: Long
) {
    fun getDayComparable(): String {
        return SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(time * 1000)
    }

    fun getDayString(): String {
        return SimpleDateFormat("dd MMM yyyy", Locale.getDefault()).format(time * 1000)
    }
}