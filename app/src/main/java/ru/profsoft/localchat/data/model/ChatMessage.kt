package ru.profsoft.localchat.data.model

import com.google.gson.Gson
import java.text.SimpleDateFormat
import java.util.*

class ChatMessage (
        val userId: String?,
        var text: String,
        time: Long
): AbstractChatMessage(time) {
    fun getMessageFormatTime(): String {
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = time * 1000
        val now = Calendar.getInstance()
        if (now.get(Calendar.DAY_OF_MONTH) == calendar.get(Calendar.DAY_OF_MONTH)) {
            return SimpleDateFormat("HH:mm", Locale.getDefault()).format(calendar.time)
        }
        if (now.get(Calendar.DAY_OF_MONTH) == calendar.get(Calendar.DAY_OF_MONTH) + 1) {
            return "вчера"
        }
        if (now.get(Calendar.YEAR) == calendar.get(Calendar.YEAR)) {
            return SimpleDateFormat("dd MMM", Locale.getDefault()).format(calendar.time)
        }
        return SimpleDateFormat("dd MMM yyyy", Locale.getDefault()).format(calendar.time)
    }

    override fun toString(): String {
        return Gson().toJson(this)
    }
}