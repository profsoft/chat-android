package ru.profsoft.localchat.data.model

class SystemMessage (
        val text: String,
        time: Long
): AbstractChatMessage(time)