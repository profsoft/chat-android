package ru.profsoft.localchat.data.model

import com.google.gson.Gson

data class User (
        var userId: String,
        var ltd: Double,
        var lng: Double
) {
    override fun toString(): String {
        return Gson().toJson(this)
    }
}