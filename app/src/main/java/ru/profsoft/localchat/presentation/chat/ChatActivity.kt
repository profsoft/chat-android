package ru.profsoft.localchat.presentation.chat

import android.Manifest
import android.location.Location
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.github.nkzawa.socketio.client.Socket
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationServices
import kotlinx.android.synthetic.main.activity_chat.*
import ru.profsoft.localchat.R
import ru.profsoft.localchat.data.model.AbstractChatMessage
import com.google.android.gms.common.GoogleApiAvailability
import android.content.pm.PackageManager
import android.graphics.Color
import android.support.v4.app.ActivityCompat
import android.view.WindowManager
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import ru.profsoft.localchat.data.model.ChatMessage
import ru.profsoft.localchat.utils.KeyboardUtil




class ChatActivity : AppCompatActivity(),
        IChatView,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private var socket: Socket? = null
    private lateinit var presenter: IChatPresenter
    private lateinit var adapter: ChatAdapter
    private var googleApiClient: GoogleApiClient? = null
    private var location: Location? = null
    private var locationRequest: LocationRequest? = null

    private val UPDATE_INTERVAL: Long = 15000
    private val FASTEST_INTERVAL: Long = 15000

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)
        //window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)

        //window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        //window.statusBarColor = Color.parseColor("#1A000000")

        presenter = ChatPresenter(this)
        KeyboardUtil(this, root_view, this)

        val layoutManager = LinearLayoutManager(this)
        layoutManager.stackFromEnd = true
        message_list.layoutManager = layoutManager
        adapter = ChatAdapter(ArrayList())
        adapter.onUserClickListener = object : ChatAdapter.OnUserClickListener {
            override fun onUserClick(v: View, position: Int) {
                message_input.setText(message_input.text.toString() + "@" + (adapter.getMessages()[position] as ChatMessage).userId + " ")
                message_input.setSelection(message_input.text.length)
            }
        }
        message_list.adapter = adapter

        btn_send.setOnClickListener {
            if (message_input.text.toString().isNotBlank()) {
                presenter.sendMessage(message_input.text.toString())
                message_input.setText("")
            }
        }

        googleApiClient = GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build()
    }

    private val PLAY_SERVICES_RESOLUTION_REQUEST = 9000

    private fun checkPlayServices(): Boolean {
        val apiAvailability = GoogleApiAvailability.getInstance()
        val resultCode = apiAvailability.isGooglePlayServicesAvailable(this)

        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
            } else {
                finish()
            }

            return false
        }

        return true
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
    }

    override fun onConnected(p0: Bundle?) {
        if (ActivityCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                &&  ActivityCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return
        }

        location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);

        //showError("Latitude : " + location?.latitude + "\nLongitude : " + location?.longitude);

        startLocationUpdates();
    }

    private fun startLocationUpdates() {
        locationRequest = LocationRequest()
        locationRequest?.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest?.interval = UPDATE_INTERVAL
        locationRequest?.fastestInterval = FASTEST_INTERVAL

        if (ActivityCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                &&  ActivityCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            showError("You need to enable permissions to display location !")
        }

        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this)
    }

    override fun onLocationChanged(p0: Location?) {
        //showError("Latitude : " + location?.latitude + "\nLongitude : " + location?.longitude)
        if (location != null) {
            presenter.updateCoordinate(location!!.latitude, location!!.longitude)
        }
    }

    override fun onConnectionSuspended(p0: Int) {
    }

    override fun onDestroy() {
        super.onDestroy()
        socket?.disconnect()
    }

    override fun onBackPressed() {
        AlertDialog.Builder(this)
                .setTitle("Выход")
                .setMessage("Вы действительно хотите покинуть чат?")
                .setCancelable(false)
                .setNegativeButton("Отмена", null)
                .setPositiveButton("Покинуть") { _, _ ->
                    finish()
                }
                .show()
    }

    override fun getActivity(): AppCompatActivity = this

    override fun addMessage(message: AbstractChatMessage) {
        empty_chat_message.visibility = View.GONE
        message_list.visibility = View.VISIBLE
        adapter.addMessage(message)
        message_list.scrollToPosition(adapter.itemCount - 1)
    }

    override fun scrollToEnd() {
        message_list.scrollToPosition(adapter.itemCount - 1)
    }

    override fun onStart() {
        super.onStart()
        presenter.onStart()
        googleApiClient?.connect()
    }

    override fun onResume() {
        super.onResume()
        if (!checkPlayServices()) {
            showError("You need to install Google Play Services to use the App properly");
        }
    }

    override fun onPause() {
        super.onPause()
        if (googleApiClient != null && googleApiClient!!.isConnected) {
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
            googleApiClient!!.disconnect();
        }
    }

    override fun onStop() {
        super.onStop()
        presenter.onStop()
    }

    override fun showError(error: String?) {
        AlertDialog.Builder(this)
                .setTitle("Ошибка")
                .setMessage(error?:"Неизвестная ошибка")
                .setPositiveButton("OK", null)
                .setCancelable(false)
                .show()
    }
}