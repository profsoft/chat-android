package ru.profsoft.localchat.presentation.chat

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_server_message.view.*
import kotlinx.android.synthetic.main.item_system_message.view.*
import kotlinx.android.synthetic.main.item_user_message.view.*
import ru.profsoft.localchat.R
import ru.profsoft.localchat.data.SocketClient
import ru.profsoft.localchat.data.model.ChatMessage
import ru.profsoft.localchat.data.model.AbstractChatMessage
import ru.profsoft.localchat.data.model.SystemMessage
import java.util.*

class ChatAdapter(private var messages: List<AbstractChatMessage>): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    companion object {
        const val TYPE_USER_MESSAGE = 1
        const val TYPE_SERVER_MESSAGE = 2
        const val TYPE_SYSTEM_MESSAGE = 3
    }
    var onUserClickListener: OnUserClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            TYPE_USER_MESSAGE ->
                UserMessageViewHolder(inflater.inflate(R.layout.item_user_message, parent, false))
            TYPE_SERVER_MESSAGE ->
                ServerMessageViewHolder(inflater.inflate(R.layout.item_server_message, parent, false))
            else ->
                SystemMessageViewHolder(inflater.inflate(R.layout.item_system_message, parent, false))
        }
    }

    override fun getItemCount(): Int = messages.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder.itemViewType) {
            TYPE_SYSTEM_MESSAGE -> {
                val systemHolder = holder as SystemMessageViewHolder
                systemHolder.itemView.system_message_text.text =
                        (messages[position] as SystemMessage).text
            }
            TYPE_SERVER_MESSAGE -> {
                val serverHolder = holder as ServerMessageViewHolder
                val message = messages[position] as ChatMessage
                if (message.text.contains("@${SocketClient.instance.userId}")) {
                    message.text = message.text.replace("@${SocketClient.instance.userId}", "")
                    serverHolder.itemView.server_message_text.setBackgroundResource(R.drawable.bg_message_direct)
                }
                serverHolder.itemView.server_message_text.text = message.text
                serverHolder.itemView.server_message_author.text = message.userId
                serverHolder.itemView.server_message_time.text = message.getMessageFormatTime()
            }
            TYPE_USER_MESSAGE -> {
                val message = messages[position] as ChatMessage
                val userHolder = holder as UserMessageViewHolder
                userHolder.itemView.user_message_text.text = message.text
                userHolder.itemView.user_message_time.text = message.getMessageFormatTime()
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        if (messages[position] is SystemMessage) {
            return TYPE_SYSTEM_MESSAGE
        } else {
            val message = messages[position] as ChatMessage
            if (message.userId == SocketClient.instance.userId) {
                return TYPE_USER_MESSAGE
            } else {
                return TYPE_SERVER_MESSAGE
            }
        }
    }

    inner class UserMessageViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)
    inner class ServerMessageViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        init {
            itemView.server_message_author.setOnClickListener {
                onUserClickListener?.onUserClick(it, adapterPosition)
            }
        }
    }
    inner class SystemMessageViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)

    fun addMessage(message: AbstractChatMessage) {
        if (messages.isNotEmpty() && messages.last().getDayComparable() < message.getDayComparable()) {
            messages += SystemMessage(message.getDayString(), Calendar.getInstance().timeInMillis / 1000)
            notifyItemInserted(messages.size - 1)
        }
        messages += message
        notifyItemInserted(messages.size - 1)
    }

    fun getMessages(): List<AbstractChatMessage> = messages

    public interface OnUserClickListener {
        fun onUserClick(v: View, position: Int)
    }
}