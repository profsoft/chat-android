package ru.profsoft.localchat.presentation.chat

import com.github.nkzawa.socketio.client.Socket
import ru.profsoft.localchat.data.SocketClient
import org.json.JSONObject
import com.github.nkzawa.emitter.Emitter
import com.google.gson.Gson
import ru.profsoft.localchat.data.model.ChatMessage
import ru.profsoft.localchat.data.model.SystemMessage
import ru.profsoft.localchat.data.model.User


class ChatPresenter(private val view: IChatView): IChatPresenter {
    private var socket: Socket? = SocketClient.instance.socket

    override fun sendMessage(text: String) {
        val message = ChatMessage(SocketClient.instance.userId, text, 0)
        socket?.emit(SocketClient.NEW_MESSAGE_KEY, message.toString())
    }

    override fun updateCoordinate(ltd: Double, lng: Double) {
        if (SocketClient.instance.userId == null) {
            return
        }
        val newUserCoord = User(SocketClient.instance.userId!!, ltd, lng)
        if (SocketClient.instance.user == null) {
            SocketClient.instance.user = newUserCoord
            socket?.emit(SocketClient.UPDATE_GPS_KEY, SocketClient.instance.user!!.toString())
        } else {
            if (newUserCoord != SocketClient.instance.user) {
                SocketClient.instance.user = newUserCoord
                socket?.emit(SocketClient.UPDATE_GPS_KEY, SocketClient.instance.user!!.toString())
            }
        }
    }

    override fun onStart() {
        socket?.on(SocketClient.NEW_USERNAME_KEY, onNewUsername)
        socket?.on(SocketClient.USER_CONNECTED_KEY, onConnectUser)
        socket?.on(SocketClient.USER_DISCONNECTED_KEY, onDisconnectUser)
        socket?.on(SocketClient.SERVER_MESSAGE_KEY, onNewMessage)
        socket?.connect()
    }

    private val onNewUsername = Emitter.Listener { args ->
        view.getActivity().runOnUiThread {
            val data = (args[0] as JSONObject).toString()
            val user = Gson().fromJson<User>(data, User::class.java)
            SocketClient.instance.userId = user.userId
        }
    }

    private val onConnectUser = Emitter.Listener { args ->
        view.getActivity().runOnUiThread {
            val data = (args[0] as JSONObject).toString()
            val systemMessage = Gson().fromJson<SystemMessage>(data, SystemMessage::class.java)
            if (!systemMessage.text.contains(SocketClient.instance.userId?:"")) {
                view.addMessage(systemMessage)
            }
        }
    }

    private val onDisconnectUser = Emitter.Listener { args ->
        view.getActivity().runOnUiThread {
            val data = (args[0] as JSONObject).toString()
            val systemMessage = Gson().fromJson<SystemMessage>(data, SystemMessage::class.java)
            view.addMessage(systemMessage)
        }
    }

    private val onNewMessage = Emitter.Listener { args ->
        view.getActivity().runOnUiThread {
            val data = (args[0] as JSONObject).toString()
            val message = Gson().fromJson<ChatMessage>(data, ChatMessage::class.java)
            view.addMessage(message)
        }
    }

    override fun onStop() {
        socket?.disconnect()
        socket?.off(SocketClient.NEW_USERNAME_KEY)
        socket?.off(SocketClient.USER_CONNECTED_KEY)
        socket?.off(SocketClient.USER_DISCONNECTED_KEY)
        socket?.off(SocketClient.SERVER_MESSAGE_KEY)
    }
}