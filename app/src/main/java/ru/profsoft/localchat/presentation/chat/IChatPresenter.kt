package ru.profsoft.localchat.presentation.chat

interface IChatPresenter {
    fun onStart()
    fun onStop()
    fun sendMessage(text: String)
    fun updateCoordinate(ltd: Double, lng: Double)
}