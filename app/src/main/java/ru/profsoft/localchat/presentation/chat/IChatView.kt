package ru.profsoft.localchat.presentation.chat

import android.support.v7.app.AppCompatActivity
import ru.profsoft.localchat.data.model.AbstractChatMessage
import ru.profsoft.localchat.data.model.ChatMessage

interface IChatView {
    fun addMessage(message: AbstractChatMessage)
    fun getActivity(): AppCompatActivity
    fun showError(error: String?)
    fun scrollToEnd()
}