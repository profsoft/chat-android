package ru.profsoft.localchat.presentation.start

import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_start.*
import ru.profsoft.localchat.R
import ru.profsoft.localchat.data.SocketClient
import ru.profsoft.localchat.presentation.chat.ChatActivity
import ru.profsoft.localchat.utils.PermissionHelper

class StartActivity: AppCompatActivity() {
    private val PERMISSIONS_REQUEST = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)
        btn_start_chat.startPulse()
        btn_start_chat.setOnClickListener {
//            if (server_address.text.toString().isBlank()) {
//                Toast.makeText(this, "Заполните адрес сервера", Toast.LENGTH_SHORT).show()
//                return@setOnClickListener
//            }
//            SocketClient.BASE_URL = server_address.text.toString()
            checkPermission()
        }
    }

    private fun checkPermission() {
        val permissions = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
        val permissionHelper = PermissionHelper(this)
        if (permissionHelper.checkPermissions(permissions, PERMISSIONS_REQUEST)) {
            startChat()
        } else {
            checkPermission()
        }
    }

    private fun startChat() {
        startActivity(Intent(this, ChatActivity::class.java))
        finish()
    }
}