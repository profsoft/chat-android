package ru.profsoft.localchat.utils

import android.app.Activity
import android.graphics.Rect
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.ViewTreeObserver
import android.view.inputmethod.InputMethodManager
import ru.profsoft.localchat.presentation.chat.IChatView

class KeyboardUtil(act: AppCompatActivity, private val contentView: View, private val view: IChatView) {
    private var decorView: View = act.window.decorView
    private var onGlobalLayoutListener: ViewTreeObserver.OnGlobalLayoutListener = ViewTreeObserver.OnGlobalLayoutListener {
        val r = Rect()
        decorView.getWindowVisibleDisplayFrame(r)
        val height = decorView.context.resources.displayMetrics.heightPixels
        val diff = height - r.bottom
        if (diff != 0) {
            if (contentView.paddingBottom != diff) {
                contentView.setPadding(0, 0, 0, diff)
                view.scrollToEnd()
            }
        } else {
            if (contentView.paddingBottom != 0) {
                contentView.setPadding(0, 0, 0, 0)
                view.scrollToEnd()
            }
        }
    }

    init {
        decorView.viewTreeObserver.addOnGlobalLayoutListener(onGlobalLayoutListener)
    }

    fun enable() {
        decorView.viewTreeObserver.addOnGlobalLayoutListener(onGlobalLayoutListener)
    }

    fun disable() {
        decorView.viewTreeObserver.removeOnGlobalLayoutListener(onGlobalLayoutListener)
    }

    companion object {

        /**
         * Helper to hide the keyboard
         *
         * @param act
         */
        fun hideKeyboard(act: AppCompatActivity?) {
            if (act != null && act.currentFocus != null) {
                val inputMethodManager = act.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
                inputMethodManager.hideSoftInputFromWindow(act.currentFocus!!.windowToken, 0)
            }
        }
    }
}